package com.dataox.demo.repository;

import com.dataox.demo.dto.UsersWithDepartmentsDTO;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class DataoxRepository {

    private final JdbcTemplate jdbcTemplate;

    public DataoxRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<UsersWithDepartmentsDTO> findUsersWithDepartments() {
        return jdbcTemplate.query(
                "select d.company_department_id as id, d.department_name as name," +
                        " d.department_description as description, d.department_date_added as dateAdded," +
                        " u.user_first_name as uFirstName, u.user_last_name as uLastName from company_department d" +
                        " left join company c on d.company_id = c.company_id" +
                        " left join user_myaccount um on um.company_id = c.company_id" +
                        " left join \"user\" u on um.user_id = u.user_id" +
                        " order by id",
                (rs, rowNum) ->
                        new UsersWithDepartmentsDTO(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getString("description"),
                                rs.getDate("dateAdded").toLocalDate(),
                                rs.getString("uFirstName"),
                                rs.getString("uLastName")
                        )
        );
    }

    public List<UsersWithDepartmentsDTO> findUserWithDepartmentsByUserId(Long user_id) {
        return jdbcTemplate.query(
                "select d.company_department_id as id, d.department_name as name," +
                        " d.department_description as description, d.department_date_added as dateAdded," +
                        " u.user_first_name as uFirstName, u.user_last_name as uLastName from company_department d" +
                        " left join company c on d.company_id = c.company_id" +
                        " left join user_myaccount um on um.company_id = c.company_id" +
                        " left join \"user\" u on um.user_id = u.user_id" +
                        " where u.user_id = ? order by id",
                new Object[]{user_id},
                (rs, rowNum) ->
                        new UsersWithDepartmentsDTO(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getString("description"),
                                rs.getDate("dateAdded").toLocalDate(),
                                rs.getString("uFirstName"),
                                rs.getString("uLastName")
                        )
        );
    }
}
