package com.dataox.demo.controller;

import com.dataox.demo.dto.UsersWithDepartmentsDTO;
import com.dataox.demo.service.DataoxService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/dataox")
public class DataoxController {

    private final DataoxService dataoxService;

    public DataoxController(DataoxService dataoxService) {
        this.dataoxService = dataoxService;
    }

    @GetMapping("/users_with_departments")
    public List<UsersWithDepartmentsDTO> findUsersWithDepartments() {
        return dataoxService.findUsersWithDepartments();
    }

    @GetMapping("/users_with_departments/{id}")
    public List<UsersWithDepartmentsDTO> findUserWithDepartmentsByUserId(
            @PathVariable("id") Long userId
    ) {
        return dataoxService.findUserWithDepartmentsByUserId(userId);
    }
}
