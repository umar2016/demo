package com.dataox.demo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class UsersWithDepartmentsDTO {

    private String userLastName;

    private String userFirstName;

    private Long departmentId;

    private String departmentName;

    private String departmentDescription;

    private LocalDate departmentDateAdded;

    public UsersWithDepartmentsDTO(
            Long departmentId,
            String departmentName,
            String departmentDescription,
            LocalDate departmentDateAdded,
            String userLastName,
            String userFirstName
    ) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.departmentDescription = departmentDescription;
        this.departmentDateAdded = departmentDateAdded;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
    }
}
