package com.dataox.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CompanyDepartmentManager {

    private Long companyDepartmentManagerId;

    private CompanyDepartment companyDepartmentId;

    private User userId;

}
