package com.dataox.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class User {

    private Long userId;

    private String userLastName;

    private String userFirstName;

}
