package com.dataox.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserMyAccount {

    private Long userMyaccountId;

    private User userId;

    private Company companyId;

}
