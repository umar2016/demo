package com.dataox.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class CompanyDepartment {

    private Long companyDepartmentId;

    private Company companyId;

    private CompanyDepartment departmentParent;

    private String departmentName;

    private String departmentDescription;

    private LocalDateTime departmentDateAdded;

    private Integer departmentActive;

    private String inviteToken;

    private Integer orderNumber;
}
