package com.dataox.demo.service;

import com.dataox.demo.dto.UsersWithDepartmentsDTO;
import com.dataox.demo.repository.DataoxRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DataoxService {

    private final DataoxRepository dataoxRepository;

    public DataoxService(DataoxRepository dataoxRepository) {
        this.dataoxRepository = dataoxRepository;
    }

    public List<UsersWithDepartmentsDTO> findUsersWithDepartments() {
        return dataoxRepository.findUsersWithDepartments();
    }

    public List<UsersWithDepartmentsDTO> findUserWithDepartmentsByUserId(Long userId) {
        return dataoxRepository.findUserWithDepartmentsByUserId(userId);
    }
}
